package com.pij.homework;

import android.app.Application;

import com.pij.homework.net.FruitSource;
import com.pij.homework.stat.ErrorSender;
import com.pij.homework.stat.StatFruitSource;
import com.pij.homework.stat.StatsService;

import retrofit.RestAdapter;

/**
 * @author Pierrejean on 13/08/2015.
 */
public class HomeworkApplication extends Application {
    private static final String ENDPOINT = "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master";

    private final RestAdapter.Builder builder = new RestAdapter.Builder();
    private final RestAdapter restAdapter = builder.setEndpoint(ENDPOINT).setLogLevel(
            RestAdapter.LogLevel.FULL).build();
    private final StatsService stats = restAdapter.create(StatsService.class);

    private final FruitSource fruitSource = restAdapter.create(FruitSource.class);
    private final FruitSource statsFruitSource = new StatFruitSource(fruitSource, stats);

    public StatsService getStatsService() {
        return stats;
    }

    public FruitSource getFruitSource() {
        return statsFruitSource;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Thread.setDefaultUncaughtExceptionHandler(new ErrorSender(stats));
    }
}
