package com.pij.homework.net;

import com.google.gson.annotations.Expose;

import lombok.Data;

/**
 */
@Data
public class Fruit {

    @Expose
    private String type;
    @Expose
    private int price;
    @Expose
    private int weight;

}
