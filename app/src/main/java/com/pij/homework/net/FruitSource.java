package com.pij.homework.net;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * @author Pierrejean on 04/08/2015.
 */
public interface FruitSource {

    @GET("/data.json")
    void retrieveFruits(Callback<FruitsResponse> response);
}
