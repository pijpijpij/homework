package com.pij.homework.net;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class FruitsResponse {

    @Expose
    private List<Fruit> fruit = new ArrayList<Fruit>();

}