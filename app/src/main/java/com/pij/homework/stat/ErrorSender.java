package com.pij.homework.stat;

import android.support.annotation.NonNull;

import com.pij.net.NoopCallback;

import static org.apache.commons.lang3.Validate.notNull;

/** Could do some refactoring around StatService and StatEventSender.
 * @author Pierrejean on 13/08/2015.
 */
public class ErrorSender implements Thread.UncaughtExceptionHandler {
    private final StatsService stats;

    public ErrorSender(@NonNull StatsService stats) {
        this.stats = notNull(stats);
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        // That should be a little more than ! (stacktrace, phone model...)
        String data = ex.getMessage();
        stats.sendStats(StatsService.EVENT_ERROR, data, new NoopCallback<Void>());
    }
}
