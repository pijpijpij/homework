package com.pij.homework.stat;

import android.support.annotation.NonNull;

import com.pij.net.DelegatingCallback;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * @author Pierrejean on 11/08/2015.
 */
class NetStatsCallback<T> extends DelegatingCallback<T> {

    @NonNull
    private final StatEventSender statCollector;

    public NetStatsCallback(Callback<T> businessCallback, @NonNull StatEventSender stats) {
        super(businessCallback);
        statCollector = notNull(stats);
    }

    @Override
    public void success(T t, Response response) {
        statCollector.sendEvent(StatsService.EVENT_LOAD);
        super.success(t, response);
    }

    @Override
    public void failure(RetrofitError error) {
        statCollector.sendEvent(StatsService.EVENT_ERROR);
        super.failure(error);
    }

}
