package com.pij.homework.stat;

import android.support.annotation.NonNull;

import com.pij.net.NoopCallback;

import retrofit.Callback;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * @author Pierrejean on 11/08/2015.
 */
public class StatEventSender {

    private final StatsService stats;
    private final long start;
    private final Callback<Void> noopCallback = new NoopCallback<>();

    public StatEventSender(@NonNull StatsService stats) {
        this.stats = notNull(stats);
        start = now();
    }

    private long now() {
        return System.currentTimeMillis();
    }

    @NonNull
    private String getElapsedInMilli() {
        return String.valueOf(now() - start);
    }

    public void sendEvent(String event) {stats.sendStats(event, getElapsedInMilli(), noopCallback);}

}
