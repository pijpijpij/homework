package com.pij.homework.stat;

import android.support.annotation.NonNull;

import com.pij.homework.net.FruitSource;
import com.pij.homework.net.FruitsResponse;

import retrofit.Callback;

import static org.apache.commons.lang3.Validate.notNull;

/** Could do some refactoring around StatService and StatEventSender.
 * @author Pierrejean on 11/08/2015.
 */
public class StatFruitSource implements FruitSource {
    private final FruitSource decorated;
    private final StatsService stats;

    public StatFruitSource(@NonNull FruitSource decorated, @NonNull StatsService stats) {
        this.stats = notNull(stats);
        this.decorated = notNull(decorated);
    }

    @Override
    public void retrieveFruits(Callback<FruitsResponse> callback) {
        final Callback<FruitsResponse> statCollector = new NetStatsCallback<>(callback, new StatEventSender(stats));
        decorated.retrieveFruits(statCollector);
    }

}
