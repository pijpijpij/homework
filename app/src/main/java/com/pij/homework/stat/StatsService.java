package com.pij.homework.stat;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * @author Pierrejean on 11/08/2015.
 */
public interface StatsService {

    String EVENT_LOAD = "load";
    String EVENT_ERROR = "error";
    String EVENT_DISPLAY = "display";

    @GET("/stats")
    void sendStats(@Query("event") String event, @Query("data") String data, Callback<Void> dummyResponse);
}
