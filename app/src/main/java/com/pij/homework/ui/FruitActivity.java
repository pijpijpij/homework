package com.pij.homework.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.pij.homework.R;
import com.pij.homework.ui.model.UiFruit;

import static org.apache.commons.lang3.Validate.notNull;

public class FruitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruit);

        if (savedInstanceState == null) {
            final IntentBuilder builder = new IntentBuilder(getIntent());
            final UiFruit fruit = builder.getFruit();
            addDetail(fruit);
        }
    }

    /**
     * That can be unit tested.
     */
    public static class IntentBuilder {
        private final String KEY_FRUIT = getClass().getName() + ".fruit";
        private final Intent intent;

        public IntentBuilder(Context context) {
            intent = new Intent(context, FruitActivity.class);
        }

        public IntentBuilder(@NonNull Intent intent) {
            this.intent = notNull(intent);
        }

        public void setFruit(@NonNull UiFruit fruit) {
            intent.putExtra(KEY_FRUIT, notNull(fruit));
        }

        public UiFruit getFruit() {
            return intent.getParcelableExtra(KEY_FRUIT);
        }

        @NonNull
        public Intent getIntent() {
            return intent;
        }
    }

    private void addDetail(UiFruit fruit) {
        Fragment fragment = FruitFragment.newInstance(fruit);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, fragment);
        transaction.commit();
    }

}
