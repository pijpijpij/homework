package com.pij.homework.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pij.homework.R;
import com.pij.homework.ui.model.UiFruit;

import java.text.NumberFormat;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class FruitFragment extends Fragment {

    public static final String ARG_FRUIT = "FruitFragment.fruit";

    private NumberFormat priceFormatter = NumberFormat.getCurrencyInstance(Locale.UK);
    private NumberFormat weigthFormatter = NumberFormat.getIntegerInstance(Locale.UK);

    @Bind(R.id.fruit)
    TextView type;
    @Bind(R.id.price)
    TextView price;
    @Bind(R.id.weight)
    TextView weight;


    public static Fragment newInstance(UiFruit fruit) {
        Bundle arguments = new Bundle();
        arguments.putParcelable(ARG_FRUIT, fruit);
        Fragment result = new FruitFragment();
        result.setArguments(arguments);
        return result;
    }

    public FruitFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fruit, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        if (savedInstanceState == null) {
            final UiFruit fruit = getArguments().<UiFruit>getParcelable(ARG_FRUIT);
            // Should do something if the arguments aren't correct.
            fillUI(fruit);
        }
    }

    private void fillUI(UiFruit source) {
        type.setText(source.getType());
        price.setText(priceFormatter.format(source.getPrice()));
        weight.setText(weigthFormatter.format(source.getWeight()));
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }
}
