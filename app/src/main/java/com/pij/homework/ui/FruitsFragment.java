package com.pij.homework.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.pij.homework.R;
import com.pij.homework.net.Fruit;
import com.pij.homework.net.FruitsResponse;
import com.pij.homework.ui.model.FruitToFruitConverter;
import com.pij.homework.ui.model.UiFruit;
import com.pij.net.DelegatingCallback;
import com.pij.net.NoopCallback;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Reloads fruits on every start. Uses a State pattern, well just because I can. Data conversion from Net to UI is done
 * in the UI layer rather than in the controller layer. Bad design!
 */
public class FruitsFragment extends ListFragment {

    @Bind(android.R.id.list)
    ListView list;

    @BindString(R.string.empty_no_fruit)
    String emptyText;

    private FruitsController controller;

    private FruitToFruitConverter converter = new FruitToFruitConverter();
    private final Callback<FruitsResponse> delegateOn = new FruitsResponseDelegate(converter);
    private final Callback<FruitsResponse> delegateOff = new NoopCallback<>();
    private final DelegatingCallback<FruitsResponse> responseDelegate = new DelegatingCallback<>(delegateOff);

    public FruitsFragment() {
    }

    /** Gets its controller via onAttach(). */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        controller = (FruitsController)activity;
    }

    @Override
    public void onDetach() {
        controller = null;
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setEmptyText(emptyText);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
        startLoadingFruits();
    }

    @Override
    public void onStop() {
        stopLoadingFruits();
        super.onStop();
    }

    /**
     * That's a bit hybrid with Butter knife, but since we are using a ListFragment, we use the whole API it offers.
     */
    @Override
    public void onListItemClick(ListView list, View v, int position, long id) {
        final UiFruit fruit = (UiFruit)getListAdapter().getItem(position);
        display(fruit);
    }

    private void startLoadingFruits() {
        setListShown(false);
        responseDelegate.setDelegate(delegateOn);
        controller.retrieveFruits(responseDelegate);
    }

    private void stopLoadingFruits() {
        responseDelegate.setDelegate(delegateOff);
    }

    private void display(UiFruit fruit) {
        controller.display(fruit);
    }

    private class FruitsResponseDelegate implements Callback<FruitsResponse> {
        private final FruitToFruitConverter converter;

        private FruitsResponseDelegate(FruitToFruitConverter converter) {
            this.converter = converter;
        }

        @Override
        public void success(FruitsResponse net, Response response) {

            List<UiFruit> ui = converter.create(net.getFruit());
            final SimpleAdapter<Fruit> adapter = new SimpleAdapter<>(getActivity(), ui);
            setListAdapter(adapter);
        }

        @Override
        public void failure(RetrofitError error) {
            setListAdapter(null);
            setListShown(true);
            //noinspection deprecation
            String message = ObjectUtils.toString(error.getBody());
            if (StringUtils.isBlank(message)) {
                message = error.getMessage();
            }
            if (StringUtils.isBlank(message)) {
                message = error.getResponse().getReason();
            }
            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Too short a time to use proper Dependency injection (Dagger), so a factory will suffice! :)
     * @author Pierrejean on 04/08/2015.
     */
    interface FruitsController {

        void retrieveFruits(Callback<FruitsResponse> listener);

        void display(UiFruit fruit);

    }
}
