package com.pij.homework.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.pij.homework.HomeworkApplication;
import com.pij.homework.R;
import com.pij.homework.net.FruitSource;
import com.pij.homework.net.FruitsResponse;
import com.pij.homework.stat.StatEventSender;
import com.pij.homework.stat.StatsService;
import com.pij.homework.ui.model.UiFruit;

import retrofit.Callback;

/**
 * Adds the fragment on creation and replaces it when a refresh is needed.
 */
public class MainActivity extends AppCompatActivity implements FruitsFragment.FruitsController {

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            addList();
        }
    }

    /** Ads the menu, with refresh. */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /** Dispatches menu actions. */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_refresh) {
            replaceList();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void replaceList() {
        Fragment fragment = new FruitsFragment();
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void addList() {
        Fragment fragment = new FruitsFragment();
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, fragment);
        transaction.commit();
    }

    @Override
    public void retrieveFruits(Callback<FruitsResponse> listener) {

        final UiStatsCallback<FruitsResponse> statsCallback = new UiStatsCallback<>(listener, new StatEventSender(
                getStatsService()));
        getFruitSource().retrieveFruits(statsCallback);
    }

    @Override
    public void display(UiFruit fruit) {
        FruitActivity.IntentBuilder builder = new FruitActivity.IntentBuilder(this);
        builder.setFruit(fruit);
        Intent intent = builder.getIntent();
        startActivity(intent);
    }

    private FruitSource getFruitSource() {
        return ((HomeworkApplication)getApplication()).getFruitSource();
    }

    public StatsService getStatsService() {
        return ((HomeworkApplication)getApplication()).getStatsService();
    }
}
