package com.pij.homework.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pij.homework.ui.model.UiFruit;

import java.util.List;

/**
 * @author Pierrejean on 10/08/2015.
 */
class SimpleAdapter<T> extends ArrayAdapter<UiFruit> {

    private final LayoutInflater inflater;

    public SimpleAdapter(Context context, List<UiFruit> objects) {
        super(context, 0, objects);
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /** Not optimized. */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View result = convertView;
        if (result == null) {
            result = createView(parent);
        }
        final TextView label = (TextView)result.findViewById(android.R.id.text1);
        label.setText(getItem(position).getType());

        return result;
    }

    private View createView(ViewGroup parent) {
        return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
    }

}
