package com.pij.homework.ui;

import android.support.annotation.NonNull;

import com.pij.homework.stat.StatEventSender;
import com.pij.homework.stat.StatsService;
import com.pij.net.DelegatingCallback;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * @author Pierrejean on 11/08/2015.
 */
public class UiStatsCallback<T> extends DelegatingCallback<T> {

    @NonNull
    private final StatEventSender statCollector;

    public UiStatsCallback(Callback<T> businessCallback, @NonNull StatEventSender stats) {
        super(businessCallback);
        statCollector = notNull(stats);
    }

    @Override
    public void success(T t, Response response) {
        super.success(t, response);
        statCollector.sendEvent(StatsService.EVENT_DISPLAY);
    }

    @Override
    public void failure(RetrofitError error) {
        super.failure(error);
        statCollector.sendEvent(StatsService.EVENT_ERROR);
    }

}
