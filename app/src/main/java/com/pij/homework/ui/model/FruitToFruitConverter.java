package com.pij.homework.ui.model;

import com.pij.homework.net.Fruit;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pierrejean on 10/08/2015.
 */
public class FruitToFruitConverter {

    public UiFruit create(Fruit net) {
        return UiFruit.create(net.getType(), net.getPrice(), net.getWeight());
    }

    public List<UiFruit> create(List<Fruit> net) {
        List<UiFruit> ui = new ArrayList<>(net.size());
        for (Fruit source : net) {
            ui.add(create(source));
        }
        return ui;
    }
}
