package com.pij.homework.ui.model;

import android.os.Parcelable;

import auto.parcel.AutoParcel;

@AutoParcel
abstract public class UiFruit implements Parcelable {

    static UiFruit create(String type, int price, int weight) {
        return new AutoParcel_UiFruit(type, price, weight);
    }

    public abstract String getType();

    public abstract int getPrice();

    public abstract int getWeight();

}
