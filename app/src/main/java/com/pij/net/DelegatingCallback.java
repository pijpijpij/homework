package com.pij.net;

import android.support.annotation.NonNull;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * Utility class.
 * @author Pierrejean on 04/08/2015.
 */
public class DelegatingCallback<T> implements Callback<T> {
    private Callback<T> delegate;

    public DelegatingCallback(@NonNull Callback<T> delegate) {
        setDelegate(delegate);
    }

    @Override
    public void success(T t, Response response) {
        delegate.success(t, response);
    }

    @Override
    public void failure(RetrofitError error) {
        delegate.failure(error);
    }

    public void setDelegate(@NonNull Callback<T> delegate) {
        this.delegate = notNull(delegate);
    }
}
