package com.pij.net;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author Pierrejean on 04/08/2015.
 */
public class NoopCallback<T> implements Callback<T> {
    @Override
    public void success(T fruitsResponse, Response response) {
        // Nothing
    }

    @Override
    public void failure(RetrofitError error) {
        // Nothing
    }
}
