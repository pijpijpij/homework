package com.pij.homework.stat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import retrofit.Callback;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * @author Pierrejean on 18/08/2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class ErrorSenderTest {

    @Mock
    private StatsService mockSender;

    @Test(expected = NullPointerException.class)
    public void test_NullService_Throws() {
        //noinspection ConstantConditions
        new ErrorSender(null);
    }

    @Test
    public void test_Call_CallsSender() {
        final ErrorSender tested = new ErrorSender(mockSender);

        tested.uncaughtException(Thread.currentThread(), new Exception("Hi!"));

        //noinspection unchecked
        verify(mockSender).sendStats(eq("error"), eq("Hi!"), any(Callback.class));
    }
}