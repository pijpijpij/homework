package com.pij.homework.stat;

import com.pij.net.DelegatingCallback;
import com.pij.net.DelegatingCallbackTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;

/**
 * @author Pierrejean on 18/08/2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class NetStatsCallbackTest extends DelegatingCallbackTest<String> {

    @Mock
    private StatEventSender mockStatSender;
    @Mock
    private Callback<String> mockDelegate;

    @Override
    protected DelegatingCallback<String> createSut(Callback<String> delegate) {
        return new NetStatsCallback<>(delegate, mockStatSender);
    }

    @Override
    protected String createDefaultResponse() {
        return "Zip!";
    }

    @Test
    public void test_Success_CallsStatSenderLoad() {
        final DelegatingCallback<String> tested = createSut(mockDelegate);
        tested.success(createDefaultResponse(), getDefaultResponse());
        verify(mockStatSender).sendEvent("load");
    }

    @Test
    public void test_Success_CallsStatSenderAfter() {
        final DelegatingCallback<String> tested = createSut(mockDelegate);
        tested.success(createDefaultResponse(), getDefaultResponse());
        final InOrder inOrder = inOrder(mockStatSender, mockDelegate);
        inOrder.verify(mockStatSender).sendEvent(anyString());
        inOrder.verify(mockDelegate).success(anyString(), any(Response.class));
    }

    @Test
    public void test_Failure_CallsStatSenderError() {
        final DelegatingCallback<String> tested = createSut(mockDelegate);
        tested.failure(RetrofitError.unexpectedError("Zip", new Exception()));
        verify(mockStatSender).sendEvent("error");
    }
}