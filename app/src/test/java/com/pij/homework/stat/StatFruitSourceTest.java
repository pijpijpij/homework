package com.pij.homework.stat;

import com.pij.homework.net.FruitSource;
import com.pij.homework.net.FruitsResponse;
import com.pij.net.NoopCallback;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import retrofit.Callback;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

/**
 * @author Pierrejean on 18/08/2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class StatFruitSourceTest {

    @Mock
    private FruitSource mockFruitSource;
    @Mock
    private StatsService mockStats;
    @Mock
    private Callback<FruitsResponse> callback = new NoopCallback<>();

    @Test(expected = NullPointerException.class)
    public void test_NullFruitSource_Throws() {
        //noinspection ConstantConditions
        new StatFruitSource(null, mockStats);
    }

    @Test(expected = NullPointerException.class)
    public void test_NullStatsService_Throws() {
        //noinspection ConstantConditions
        new StatFruitSource(mockFruitSource, null);
    }

    @Test
    public void test_RetrieveFruit_CallsDelegate() {
        final StatFruitSource tested = new StatFruitSource(mockFruitSource, mockStats);
        tested.retrieveFruits(callback);

        //noinspection unchecked
        verify(mockFruitSource).retrieveFruits(any(Callback.class));
    }

    @Test
    public void test_RetrieveFruitResult_SendsLoadEvent() {
        final StatFruitSource tested = new StatFruitSource(mockFruitSource, mockStats);
        final Answer<FruitsResponse> answer = new Answer<FruitsResponse>() {
            @Override
            public FruitsResponse answer(InvocationOnMock invocation) throws Throwable {
                //noinspection unchecked
                ((Callback<FruitsResponse>)invocation.getArguments()[0]).success(new FruitsResponse(), null);
                return null;
            }
        };
        //noinspection unchecked
        doAnswer(answer).when(mockFruitSource).retrieveFruits(any(Callback.class));
        tested.retrieveFruits(callback);

        //noinspection unchecked
        verify(mockStats).sendStats(eq("load"), anyString(), any(Callback.class));
    }
}