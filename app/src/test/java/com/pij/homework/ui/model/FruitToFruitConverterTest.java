package com.pij.homework.ui.model;

import android.support.annotation.NonNull;

import com.pij.homework.net.Fruit;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Pierrejean on 11/08/2015.
 */
public class FruitToFruitConverterTest {

    private FruitToFruitConverter tested;

    @Before
    public void setUp() {
        tested = new FruitToFruitConverter();
    }

    @NonNull
    private Fruit createFruit1() {
        final Fruit net = new Fruit();
        net.setType("ha!");
        net.setPrice(2);
        net.setWeight(170);
        return net;
    }

    @NonNull
    private Fruit createFruit2() {
        final Fruit net = new Fruit();
        net.setType("toto");
        net.setPrice(180);
        net.setWeight(20);
        return net;
    }

    @Test(expected = NullPointerException.class)
    public void test_NullFruit_Throws() {
        tested.create((Fruit)null);
    }

    @Test(expected = NullPointerException.class)
    public void test_NullList_Throws() {
        tested.create((List<Fruit>)null);
    }

    @Test
    public void test_SimpleFruit_IdenticalFields() {

        final Fruit net = createFruit1();

        final UiFruit result = tested.create(net);

        assertThat(result.getType(), is("ha!"));
        assertThat(result.getPrice(), is(2));
        assertThat(result.getWeight(), is(170));
    }


    @Test
    public void test_2input_2output() {
        List<Fruit> input = Arrays.asList(createFruit1(), createFruit2());
        final List<UiFruit> result = tested.create(input);

        assertThat(result.size(), is(2));
    }
}