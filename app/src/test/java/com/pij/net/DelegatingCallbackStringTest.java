package com.pij.net;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.mime.TypedString;

import static org.mockito.Mockito.verify;

/**
 * @author Pierrejean on 18/08/2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class DelegatingCallbackStringTest {

    @Mock
    private Callback<String> mockDelegate;
    private Response defaultResponse = new Response("zip", 200, "reason", new ArrayList<Header>(),
                                                    new TypedString("tada!"));

    @Test(expected = NullPointerException.class)
    public void test_NullDelegate() {
        //noinspection ConstantConditions
        new DelegatingCallback<String>(null);
    }

    @Test
    public void test_Success_CallsSuccess() {
        DelegatingCallback<String> tested = new DelegatingCallback<>(mockDelegate);

        tested.success("ha!", defaultResponse);

        verify(mockDelegate).success("ha!", defaultResponse);
    }

}