package com.pij.net;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.mime.TypedString;

import static org.mockito.Mockito.verify;

/**
 * @author Pierrejean on 18/08/2015.
 */
@RunWith(MockitoJUnitRunner.class)
abstract public class DelegatingCallbackTest<T> {

    @Mock
    private Callback<T> mockDelegate;

    private Response defaultResponse = new Response("zip", 200, "reason", new ArrayList<Header>(),
                                                    new TypedString("tada!"));

    protected abstract DelegatingCallback<T> createSut(Callback<T> delegate);

    protected abstract T createDefaultResponse();

    protected final Response getDefaultResponse() {
        return defaultResponse;
    }


    @Test(expected = NullPointerException.class)
    public void test_NullDelegate() {
        createSut(null);
    }

    @Test
    public void test_Success_CallsSuccess() {
        DelegatingCallback<T> tested = createSut(mockDelegate);

        final T response = createDefaultResponse();
        tested.success(response, defaultResponse);

        verify(mockDelegate).success(response, defaultResponse);
    }


    @Test
    public void test_Failure_CallsFailure() {
        DelegatingCallback<T> tested = createSut(mockDelegate);

        final RetrofitError error = RetrofitError.unexpectedError("url", new Exception());
        tested.failure(error);

        verify(mockDelegate).failure(error);
    }

}